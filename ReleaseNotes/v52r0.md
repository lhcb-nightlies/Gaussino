!2017-12-12 Gauss v52r0
===

## Release for Sim10 Development incompatible with Sim09 and Sim09-Upgrade
i.e. different version of generators and new technical features

This version uses projects LHCb v42r6p2, Geant4 v96r4p7, Gaudi v28r2, LCG_88
 (HepMC 2.06.09, Root 6.08.06)  
and data packages SQLDDDB v7rX , ParamFiles v8rX , FieldMap v5rX ,
 AppConfig v3rX , Geant4Files v96rX , GDMLData v1rX , XmlVis v2rX , 
 DecFiles v30rX , LHAPDFSets v2rX , BcVegPyData v2rX , GenXiccData v3rX , 
 PGunsData v1rX , MIBData v3rX

This version uses (via LCG_88/MCGenerators):  
 __pythia8 230 (with OLD LHCb Tune1)__, __lhapdf 6.1.6.cxxstd__, photos++ 3.56, 
 tauola++ 1.1.6b.lhcb, pythia 6.427.2,
 hijing 1.383bs.2, crmc 1.5.6 (epos), alpgen 2.1.4, powhegbox r3043.lhcb, 
 herwig++ 2.7.1, __thepeg 2.0.3__, 
 __rivet 2.5.2__, yoda 1.5.9  
and the internal implementation of:
 EvtGen with EvtGenExtras, AmpGen, Mint;
 BcVegPy and GenXicc, SuperChic, LPair, MIB

This version is released on **master**: the last release on trunk on svn on which this started from is **v50r0**.

This release is the first of the **Sim10-Dev01** productions for tuning.
Beside Sim10-Dev specific changes (support for Pythia8 230 and Generators FSRs) for the rest is 
almost identical to Gauss v51r0. 
As such it ONLY supports the latest fast RICH new database tags (i.e. dddb-20171030{,-1,-2,-3}) 
and for the upgrade only the latest SciFi geometry (i.e. dddb-20171126 and sim-20171127-vc-md100).

This is the first version released on master in Git, the last one release on svn trunk was v50r0; 
as such the release notes start from it and have all last svn commit as well as all Merge Request, 
including the one porting all development from the Sim09 branch to v49r7. 
MR corresponding to other MR are indicated.


## Developemts for Sim10 (including in svn head before switching to Git) or specific fixes

#### Generators

**[MR !214] Sim10Dev: Change version of Pythia8 to latest 230**  
This version supports Heavy Ion production as well as many other improvements, refer to
Pythia8 release notes.

**[MR !138] New version of Pythia8, 226, in view of Sim10**  
    
**[MR !168] Fix genfsr location for spillover** 
by D. Fazzini  

**[MR !115] Fixed setting LHAPDF in LbPythia8 for PYTHIA8r2xx**   

**Introduce Generators File Summary Records (GenFSR) for Generators statistics**  
This was committed to svn.  
It consists of
- [Commit eb3ea972] Davide
- [Commit 9c712f81] Update Pythia8Production.cpp to store the Pythia8 counters in genFSR  
- [Commit 5d82d028] Update PythiaProduction.cpp to store the Pythia counters in genFSR  
- [Commit c10d7e92] Added the possibility to store the generator counters in genFSR 
- [Commit 66b55299] Update Configurable to write and merge genFSR

 
#### Geometry and Geant4

**[MR !201] Neutron shield for Fiber Tracker in default geometry of upgrade in Gauss configurable and latest tags**  
Corresponds to !192 in Sim09-upgrade  
Also include changes to use latest database tags in example options (see merge request !188 (merged)
 + change of DDDB tag)  
The new dbase tags are only compatible with LHCb > v42r4 and are: ```dddb-20171010``` and 
```sim-20170301-vc-md100``` or higher  
    
**[MR !200] Update options and tests for upgrade detector**  
This request is a cherry pick of all commits but one of in MR !167 in Sim09-upgrade

**[MR !143] Update GaussGeo to report & skip invalid geo objects**  
Corresponds to !173 in Sim09-upgrade  

**[MR !117] Utilise GaussGeo in Gauss Configurable**   
Corresponds to !171 in Sim09-upgrade
    

#### Monitoring and tests

**[MR !223] Update qmtest references and remove obsolete tests from GaussSys**  

**[MR !215] Fixed QMtest related to the GenFSR**  

**[MR !51] Adding tests for genFSR**  

**[MR !144] Update DBTags for upgrade baseline QMTests**  
This does the same as !188 in Sim09-upgrade
  
**[Commit 21c012f2] Fixed MCVertexPosZX plot and added new plot of distance traversed in silicon**  
Ported to Sim09-upgrade as MR !152
- Edit VPGaussMoni.cpp to fix MCVertexPosZX (was plotting ZY)
- Add new plot showing distance traversed in silicon (SiDist)


#### Code optmization, modernization and maintenance

**[MR !212] Update dddb tags with latest fix for faster RICH**  
Correspond to !210 in Sim09-upgrade.  
Needs !205.  

**[MR !205] Fix to DDDB update for Mirror reflectivity**
This request is to update GaussRICH to use the recent fix in DDDB-Update in the Mirror reflectivity table usage.     
Related to LHCBGAUSS-1233.  
Corresponds to !211 in Sim09-upgrade

**[MR !186] Remove all cmt files**  
This does the same as !187 in Sim09-upgrade
    
**[MR !146] Added GaussGeo for Compatible Versions of Gauss**  
Related to LHCBGAUSS-869  
Corresponds to !185 in Sim09-upgrade

**[MR !116] Obsolete physics lists cleanup**  
Corresponds to !182 in Sim09-upgrade
 
**[MR !76] GaussCherenkov Update for Cherenkov Hit Time studies** 
This is included in !142 in Sim09-upgrade  
    
**[MR !114] Update CMakeLists.txt to add dependency of RICH packages for run time**  
Corresponds to !154 in Sim09-upgrade
    
**[Commit 4212784d and 52c3744d] Fix problem in GaussRICH at run time**
Update Configuration.py to remove import of Gauss outside the scope to the configurable itself**

**[MR !34] Adapt to move of headers to RichUtils**  
Corresponds to !180 in Sim09-upgrade 

**[MR !107] Fixed Rivet::Analysis pointer in GenTune**  
Corresponds to !181 in Sim09-upgrade

**[MR !110] Compilation fixes for LHCb v42r4**  
Additional compilation errors found when moving to LHCb v42r4  
Corresponds to !179 in Sim09-upgrade

**[MR !103] Fixes in view of Sim10Dev when using Gaudi v28 and LHCb v42r3**  
Replace endreq that is no longer available in Gaudi v28 with endmsg in many GaussRICH and 
GaussCherenkov files as well as in some Gen packages (LbCRMC, LbHijing, LbHidValley, Generators, 
LbPythia)
Corresponds to !147 in Sim09-upgrade

**[MR !33] follow changes in LHCb!301**   
see LHCb!301 (merged) and Marco's comment on LHCb@2754c33b  
Corresponds to !178 in Sim09-upgrade
    
**[Commit faa26f16 and b581085b] Add README.md to Gauss project**  

**[Commit 7681e762] GaussRICH**  
git-svn-id: svn+ssh://svn.cern.ch/reps/lhcb/Gauss/trunk@207347 4525493e-7705-40b1-a816-d608a930855b
- Added some safety features for backward compatibilty in GiGaPhysConstructorHpd for using the old options file
- Added some safety features in RichG4TrackActionRich2Photon.cpp
- For 2015 and 2016 configuration, kept the default for RICH2  scintillation fraction as that in 2012, until more precise values are known.

**[MR !55] Improve generators versions list**  
 - moved list of generators versions to a dedicated file  
 - small update to Pythia8 lookup  
 - updated LHAPDF version  
Corresponds to !184 in Sim09-upgrade


## Developments and fixes merged from or in sync with Sim09 branch 

**[MR !104] Port all Sim09 development to master**  
It includes the MR listed below and applied to Sim09 unless listed explicitely as
cherry-pick or equivalent to other MRs.

### Changes and Updates to Generators

**[MR !155] Change major version of DecFiles and match new database tags**  
Corresponds to !151 in Sim09 and !156 in Sim09-upgrade  


#### Cuts

**[MR !80] LHCBGAUSS-1023: Add filter on PT in Xicc generator cuts**  
Add property for filtering on daughter PT for Xicc generator level cuts in XiccDaughtersInLHCbAndWithMinPT.cpp
  
**[MR !74] LHCBGAUSS-1005: LambdacD cut for GenCuts**  
The goal is to create the DaughtersInLHCbAndCutsForLambdacD after the DaughtersInLHCbAndCutsForDstarD one.

**[MR !20] New cut for *3pi analysis. Code provided by Guy Wormser, see LHCBGAUSS-844**  
Corresponds to !18 in Sim09    

#### Decay: EvtGen, EtvGenExtras, AmpGen

**[MR !129] Register VTOSLL model** 
Details in LHCBGAUSS-1149  
Corresponds to !118 in Sim09 and Sim09-upgrade

**[MR !122] Addition of BeautyTo2CharmTomu3h and BeautyTomuCharmTo3h tools to Gen/GenCuts**
Details in LHCBGAUSS-1081   
Corresponds to !92 in Sim09 and Sim09-upgrade
    
**[MR !100] Gencuts code for Bc_JpsiTauNu and background MC**   
Corresponds to !137 in Sim09 and !153 Sim09-upgrade
    
**[MR !124] Added parameter in Evtbs2llGammaISRFSR to cut on dimuon mass**  
Details in LHCBGAUSS-1089
Corresponds to !97 in Sim09 and  Sim09-upgrade

**[MR !126] Generalise and simplify daughter ordering in EvtDDalitz**
Details in LHCBGAUSS-1088  
Corresponds to !99 in Sim09 and  Sim09-upgrade

**[MR !125] Fixing bug which fails to recognise Bs --> K*bar mu mu as b --> dll transition**   
Details in LHCBGAUSS-1097
Corresponds to !98 in Sim09 and  Sim09-upgrade

**[MR !121] Update particle properties in Pythia8 - EvtGen**  
Details in LHCBGAUSS-1031
Corresponds to !88 in Sim09 and Sim09-upgrade
 
**[MR !53] LHCBGAUSS-937: New Bs to J/psi K K model, EvtBsMuMuKK**  
This contains the new EvtGen model named EvtBsMuMuKK for generating Bs to J/psi K K decays for Sim09.  
This will also be suitable for the master branch.

**[MR !45] Updates to the EvtFlatQ2 EvtGen model for B to X lepton lepton decays**  
See LHCBGAUSS-911  
The EvtFlatQ2 model has been extended to work for any B to X lepton lepton decays to generate a flat q2 distribution  

**[MR !72] LHCBGAUSS-980: fix wrong uncertainty of fit fractions in AmpGen**  
To merge also into Sim09-upgrade as well as master

**[MR !52] LHCBGAUSS-910: Added D02KKpipi model to AmpGen**  
Add the D02KKpipi model to AmpGen in Sim09 and also into the master branch.

**[MR !28] Modifications to EvtTauolaEngine for the B to tau tau physics analysis**  
Code changes to `EvtTauolaEngine` in EvtGen to allow the B to tau tau physics analysis to move forward:    
See LHCBGAUSS-841 

1. It uses the updated BaBar hadronic currents by default. An extra decay file keyword `TauolaCurrentOption` can be used to 
reset the integer value via `Tauola::setNewCurrents()`. This means that if there is a build of Tauola with the intricate CLEO 
currents enabled, then the code will be able to use that by setting
    `Define TauolaCurrentOption -1` in the decay file.
2. The code also allows the option to set the sub-branching fractions of certain modes via `Tauola::setTaukle`, which will
improve MC generation efficiency by ignoring unneeded modes, using the decay file keywords `TauolaBRx`, where x = 1,2,3 or 4.
3. It also contains some code to artificially set the PDG ids of the tau parents to vector bosons to use internal Tauola 
spin matrix weights, but this is not enabled by default, since we believe it doesn't do the right spin physics for B decays (we need 
further feedback from the Tauola developers).
4. There is also new code in `EvtParticle` to allow the future storage and retrieval of double quantities, such as amplitude 
weights, via the `setAttributeDouble(name, value)` and `value = getAttributeDouble(name)` functions, although no decay model is
using this at the moment and will therefore have zero effect on current MC production.
5. Uses new tauola 1.1.6b.lhcb to allow access to the Cleo intricate model

**[MR !17] Code changes to allow polarization of any charmonium vector particles**  
Changes in Gen/Generators/src/component/EvtGenDecay.cpp  
Details in LHCBGAUSS-831     
Correspond to MR !15 in Sim09

**[MR !19] Modified EvtHQET2FF in EvtGen for LHCBGAUSS-814**  
Corresponds to !16 in Sim09


**New code for AmpGen and LbAmpGen**  
Committed in svn and ported to Sim09 (as MR !41 and !26) and to Sim09-upgrade (as MR !44 and !29)
New Amplitude model from Tim Evans.  
It consist of two package Gen/AmpGen and Gen/LbAmpGen available in EvtGen via EvtGenExtras.
Changes in requirements and cmake build added (made with Marco Clemencic).  
For details see LHCBGAUSS-905 and LHCBGAUSS-838  
It consists of:
- [Commit f46aed4da1f] Added possibility of gen cuts to toy MC generator, moved binning and event type stuff from headers into implementations
- [Commit 9fcf549c0c0] Add options and 2008 PDG
- [Commit 29a753c343] Fixed minor bug in IVertex introduced when moving Tensor implementations
- [Commit 17b1638cdd] Moved some Tensor implementation stuff out of header
- [Commit aab6f5b9389] Added AmpGen interface
- [Commit c124b4038b] Changed models -> options, fixed path for PDG data
- [Commit ad761ec8fd] Add 2008 PDG
- [Commit af6e557551] Minor bug fixes, separated expression implementation from headers
- [Commit baed30390] Committed first models
- [Commit 966e77530] Added some example options and PDG
- [Commit 9cb18a0e3] Removed IMinisiable abstraction layer, recommited Utils
- [Commit a2d1b3ca8] Fixed utilities
- [Commit 4c60ebd81] Merged Utilities
- [Commit 70ad0fce] Add ConvertToSourceCode.cpp
- [Commit 39ce059b] Add new faster generator method
- [Commit e44ab114] First import of project srcs, apps, headers
- [Commit 8ffcfa05 and 1a629e92] first import of package Gen/AmpGen under Gauss


#### Production: Pythia8, Particle Gun, EPOS, etc...

**[MR !25] Fix for min/max particle masses**  
Bug fix that has as effect to truncate mass of Bs excited states 
See LHCBGAUSS-857

**[MR !160] Add tilt option for particle gun**
Corresponds to !134 in Sim09 and !161 in Sim09-upgrade
 
**[MR !133] Fix pgun energy according to what set in Gauss().Beam**
Details in LHCBGAUSS-1155  
Corresponds to !131 in Sim09 and  Sim09-upgrade

**[MR !119] Fix MomentumSpectrum to not generate the same particle twice**   
Closes LHCBGAUSS-842
Corresponds to !132 in Sim09 and  Sim09-upgrade

**[MR !77] Fix setting of seed in MomentumSpectrum**  
Details in LHCBGAUSS-842  
Corresponds to !128 in Sim09 and  Sim09-upgrade

**[MR !158] Remove EPOS obsolete options**  
Corresponds to !150 in Sim09 and !159 in Sim09-upgrade

**[MR !123] Fix typo in EPOS.py (LbCRMC package)**  
Details in LHCBGAUSS-1083   
Corresponds to !93 in Sim09 and  Sim09-upgrade
    
**[MR !81] LHCBGAUSS-1021: Improvement of signal embedding**  
Improve signal embedding by allowing signal particles with displaced vertices with respect to the primary vertex.
Needed for production of signal with EPOS.
  
**[MR !73] LHCBGAUSS-952: Improve EPOS options**  
Improve the EPOS.py option file to generate charmonium, bottomium, Z , W and Drell Yan

**[MR !57] LHCBGAUSS-772: PbPb peripheral events by default**  
Generate by default peripheral PbPb events

**[MR !42] Improve ImpactParam_PERIPH.py option file**  
related to LHCBGAUSS-657 and LHCBGAUSS-894 

 - To be propagated to master and Sim09 branches
 - Improvements have been made in the ImpactParam_PERIPH.py option files which purpose is to define centrality cuts in EPOS 
for PbPb.
 - Those changes have been made to account for all possible configurations of EPOS which have been deployed in the new EPOS.py
file

**[MR !35] New EPOS option file and code for fixed target configuration**  
See LHCBGAUSS-894 
Add new EPOS.py generic option file in Gen/LbCRMC which creates automatically the correct configuration for EPOS based on:

 - beam momentum to define the fixed target or collisionner mode
 - event type to generated minimum bias events or to embed Pythia8 signal events with EPOS underlying event

**[MR !14] J/Psi with EPOS**  
New options to produce J/Psi with Pythia8 and merge them in EPOS. Clean up of unused files.

**[MR !13] Avoid crashes in EvtGen with EPOS**  
Fixing the energy of the particle to avoid crash in EvtGen, see LHCBGAUSS-772

**[MR !9] fix bug for LbCRMC cmake build**  
ported to Sim09 in a different commit

**[Commit 756c69c02] add hepmc to cmake link in LbCRMC**  
Committed to svn.  
Ported to Sim09 and Sim09-upgrade via checkout and commit.  

**[Commit c24b45b8] Fix the table path in EPOS_CMS.py and EPOS_TARGET.py for production environment**  
committed to svn and ported to Sim09 with checkout and commit

**[MR !36] Add BeamSpotMarkovChainSampleVertex**  
Adds a new beamspot smearing tool that implements a fully correlated (x,y,z,t) sampling using a Markov Chain 
sampler and a full 4D PDF that describes the intersection of two bunches.  
Refers to LHCBGAUSS-906
Corresponds to !60 in Sim09 and !37 in Sim09-upgrade

**Fixes for SuperChic**
Committed in svn and the ported to Sim09 (as MR !157) and Sim09-upgrade (as MR !163)
It consists of:
- [Commit 3ead96fe]Updated documentation for proper Doxygen formatting  
- [Commit cba5c7f2]Updated indexing bug (non-issue) and source location.  
-- Fixed indexing bug in Fortran.py (does not change behaviour)  
-- Modified SuperChic.py to pick up source from legacy URL  

**[Commit d65b2fb5] update of the Hijing configuration file to be compatible with the new HI Gauss version** 
Ported to Sim09 as MR !166 and Sim09-upgrade as !165 

**[Commit 62c8e69a]Solve GenTune project dependencies for cmake**  
Committed to svn. Also in Sim09 and Sim09-upgrade with different commit.
- Add required FastJet dependency to cmake file. Remove no longer needed AIDA dependency.

**[Commit e7b6e22a] add yoda to list of packages**  
Committed to svn.  
Ported to Sim09 and Sim09-upgrade via checkout and commit.  

**[Commit 917bc3dd] add fix for HepMC in cmake in LbBound**  
Committed to svn.  
Ported to Sim09 and Sim09-upgrade via checkout and commit.  

### New fast Simulation Options

**[MR !120] ReDecay support for SignalRepeatedHadronization**  
Details in LHCBGAUSS-1041  
Corresponds to !85 in Sim09 and  Sim09-upgrade

**[MR !58] LHCBGAUSS-955: Re-use of the underlying event**  
This merge request contains the code for re-using the underlying event and only simulating the signal parts again. 
The main part of the code exists in its own package `Sim/GaussRedecay` with only minimal changes to existing parts of Gauss except 
the main Gauss Configurable. 

For running the code and remaining issues look in the merge request itself.

#### Slides to clarify the procedure  
- <a href="https://indico.cern.ch/event/588836/contributions/2374451/attachments/1377416/2092184/reuse_update.pdf"> Fast simulation meet
ing 24.11.2016</a>
- <a href="https://indico.cern.ch/event/576720/contributions/2334442/attachments/1354566/2046421/redecay_report_13_10_16.pdf">Fast simul
ation meeting 13.10.2016</a>
- <a href="https://indico.cern.ch/event/442259/contributions/2159561/attachments/1270722/1882889/available_fast_mc.pdf">A&S week 10.5.20
16</a>
- <a href="https://indico.cern.ch/event/505110/contributions/1183150/attachments/1256852/1855758/SignalRedecay.pdf">Simulation meeting 1
2.4.2016</a>

### Monitoring and tests

**[MR !209] Fixed Segmentation Fault caused by Index Issue in RadLengthColl within GaussTools**  
Related to LHCBGAUSS-876
Correspond to !189 in Sim09 and !213 in Sim09-upgrade  

**[MR !220] Removes duplicated files in MuonMoniSim already located elsewhere**  
Related to LHCBGAUSS-874
Correspond to MR !197 in Sim09 and !218 in Sim09-upgrade
    
**[MR !219] Port changes to add running over different EM physics lists**  
Correspond to MR !109 in Sim09

**[MR !196] Fixed RadLength Test and Added GaussGeo Options**  
Related to LHCBGAUSS-876
Correspond to !216 in Sim09 and !217 in Sim09-upgrade  
    
**[MR !135] Fix missing configurables for upgrade Cherenkov algorithms**  
Corresponds to !139 in Sim09 and !148 in Sim09-upgrade
   
**[MR !130] Tests for Em think layers and Brems**  
Details in LHCBGAUSS-1120  
Corresponds to !108 in Sim09 and  Sim09-upgrade

**[MR !127] Updated Labelling of Plots and Unlocked further plots**  
Details in LHCBGAUSS-869   
Corresponds to !105 in Sim09 and  Sim09-upgrade
 
**[MR !82] Updates to Hadronic Multiplicity and Cross Section Test**.  
See LHCBGAUSS-869

**[MR !68] LHCBGAUSS-874: Muon/MuonMoniSim**  
Adds options file to run Muon Hit and MultipleScattering Checkers in Gauss and a script runmuontest.py to run Gauss using these 
options. 
Tested locally and working with CMT and CMake.
    
**[MR !71] LHCBGAUSS-878: Matteo's update to the MonitorTiming for Sim09**  
Latest revision of the detailed timing test (MonitorTiming) for Sim09
    
**[MR !62] Simchecks package add missing aliases**  

**[MR !63] Fix simchecks radlength test**  

### Code modernization

**[MR !149] Port changes from MR Sim09-83 to move XmlVis to a datapackage**  
This does the same as !83 in Sim09

**[MR !31] Remove script and template for new event type tests from Gauss as well as its execution**  
Remove script and template for new event type tests.  
Remove executing the script from the cmt requirements.  This in not only no longer necessary since they are done in
 dedicated DecFilesTests project but it also creating problems in making the web page test reporting when building
 with cmake   
Details in LHCBGAUSS-887  
Corresponds to MR !27 in Sim09 and !30 in Sim09-upgrade   

